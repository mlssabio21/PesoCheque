package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pharking.pesocheque.utils.Constants;
import com.pharking.pesocheque.utils.Helper;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TransferFunds1Activity extends AppCompatActivity {

    private Spinner spSelectBank, spTerms;
    private EditText etEnterAmount;
    private String[] banks = { "Select Bank", "UnionBank" };
    private String[] terms = { "Select Terms", "One Time", "3 months", "6 months", "9 months", "12 months", "24 months", "36 months" };
    private ArrayAdapter<String> banksAdapter, termsAdapter;
    private TextView tvComputation;
    private double quotient;
    private Button btNext;

    SharedPreferences shared;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds1);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        edit = shared.edit();

        spSelectBank = (Spinner) findViewById(R.id.spSelectBank);
        spTerms = (Spinner) findViewById(R.id.spTerms);
        etEnterAmount = (EditText) findViewById(R.id.etEnterAmount);
        tvComputation = (TextView) findViewById(R.id.tvComputation);
        btNext = (Button) findViewById(R.id.btNext);

        banksAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, banks);
        spSelectBank.setAdapter(banksAdapter);
        termsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, terms);
        spTerms.setAdapter(termsAdapter);

        spTerms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(etEnterAmount.getText().toString().length() < 1) {
                    etEnterAmount.requestFocus();
                    etEnterAmount.setError("Please enter amount");
                } else {
                    DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
                    Date date = new Date();
                    Calendar c1 = Calendar.getInstance();
                    Calendar c2 = Calendar.getInstance();

                    c1.setTime(date);
                    c1.add(Calendar.MONTH, 1);

                    switch(position) {
                        case 0:
                            Toast.makeText(TransferFunds1Activity.this, "Select a term first", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            tvComputation.setText("That would be " +
                                    Helper.formatter.format(Double.valueOf(etEnterAmount.getText().toString())) + " pesos " +
                                    " this " + dateFormat.format(date));
                            break;
                        case 2:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 3;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 3);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                        case 3:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 6;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 6);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                        case 4:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 9;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 9);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                        case 5:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 12;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 12);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                        case 6:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 24;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 24);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                        case 7:
                            quotient = Double.valueOf(etEnterAmount.getText().toString()) / 36;
                            c2.setTime(date);
                            c2.add(Calendar.MONTH, 36);
                            tvComputation.setText("That would be " + Helper.formatter.format(quotient) + " pesos " +
                                    " per month for " + adapterView.getSelectedItem().toString() + " starting on " +
                                    dateFormat.format(c1.getTime()) + " to " + dateFormat.format(c2.getTime())
                            );
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spSelectBank.getSelectedItemId() == 0) {
                    Toast.makeText(TransferFunds1Activity.this, "Select a bank first", Toast.LENGTH_SHORT).show();
                } else {
                    edit.putString(Constants.QUOTIENT, Helper.formatter.format(quotient));
                    edit.putString(Constants.AMOUNT, Helper.formatter.format(Double.valueOf(etEnterAmount.getText().toString())));
                    edit.putString(Constants.MONTHS, spTerms.getSelectedItem().toString());
                    edit.commit();
                    Intent i = new Intent(TransferFunds1Activity.this, TransferFunds2Activity.class);
                    startActivity(i);
                }
            }
        });

    }
}
