package com.pharking.pesocheque;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.pharking.pesocheque.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TransferFunds3Activity extends AppCompatActivity implements SurfaceHolder.Callback {

    private TextView tvContent;
    private ImageView svVideo;
    private Button btSend;
    private String amount, terms, recipient, bank, accountNumber, currentDate, currentLocation;
    private Camera camera;
    MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;

    private static final int CAMERA_REQUEST = 1888;

    SharedPreferences shared;
//    SharedPreferences.Editor edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds3);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        tvContent = (TextView) findViewById(R.id.tvContent);
//        svVideo = (SurfaceView) findViewById(R.id.svVideo);
        svVideo = (ImageView) findViewById(R.id.svVideo);

        DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Date date = new Date();

        tvContent.setText("I am authorizing this " + shared.getString(Constants.REASONS, "") + " amounting to " +
            shared.getString(Constants.QUOTIENT, "") + " for " + shared.getString(Constants.MONTHS, "") + " to " +
                shared.getString(Constants.RECIPIENT, "") + " as of today " + dateFormat.format(date) + " at "
                + "Meralco Ave., Pasig City, Philippines"
        );

//        ActivityCompat.requestPermissions(this,
//                new String[]{Manifest.permission.CAMERA}, REQUEST_VIDEO_CAPTURE);


        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);

        getLocation();

//        startRecordingVideo();
//        startRecording();

        btSend = (Button) findViewById(R.id.btSend);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransferFunds3Activity.this, TransferFunds4Activity.class);
                startActivity(i);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            svVideo.setImageBitmap(photo);
        }
    }

    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST+3;

    private void getLocation() {

        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST);

            ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

            LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location!=null){
                double longitude = location.getLongitude();
                double latitude = location.getLatitude();
                String locLat = String.valueOf(latitude)+","+String.valueOf(longitude);
                Log.e("Activity", "latitude: " + latitude);

                Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
                StringBuilder builder = new StringBuilder();
                try {
                    List<Address> address = geoCoder.getFromLocation(latitude, longitude, 1);
                    int maxLines = address.get(0).getMaxAddressLineIndex();
                    for (int i=0; i<maxLines; i++) {
                        String addressStr = address.get(0).getAddressLine(i);
                        builder.append(addressStr);
                        builder.append(" ");
                    }

                    String finalAddress = builder.toString(); //This is the complete address.
                    Toast.makeText(this, finalAddress, Toast.LENGTH_SHORT).show();
                    Log.e("Activity", "" + finalAddress);
                } catch (IOException e) {
                    Log.e("Activity", "Error: " + e.toString());
                }
            }
        } catch (Exception e) {
            Log.e("", "" + e.toString());
        }

    }

//    private static final int VIDEO_CAPTURE = 101;
//    Uri videoUri;
//    public void startRecordingVideo() {
//        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
//            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//            File mediaFile = new File(
//                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.mp4");
//            videoUri = Uri.fromFile(mediaFile);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
//            startActivityForResult(intent, VIDEO_CAPTURE);
//        } else {
//            Toast.makeText(this, "No camera on device", Toast.LENGTH_LONG).show();
//        }
//    }

//    static final int REQUEST_VIDEO_CAPTURE = 1;

//    private void dispatchTakeVideoIntent() {
//        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
//        }
//    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

}