package com.pharking.pesocheque.utils;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by say13 on 23/11/2016.
 */

public class Helper {

    static Pattern emailPattern = Pattern.compile("[a-zA-Z0-9[!#$%&'()*+,/\\-_\\.\"]]+@[a-zA-Z0-9[!#$%&'()*+,/\\-_\"]]+\\.[a-zA-Z0-9[!#$%&'()*+,/\\-_\"\\.]]+");

    public static boolean isValidEmail(String email)
    {
        Matcher m = emailPattern.matcher(email);
        return !m.matches();
    }

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");



}
