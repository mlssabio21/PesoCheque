package com.pharking.pesocheque.utils;

/**
 * Created by say13 on 27/11/2016.
 */

public class Constants {

    public static final String PREF_NAME = "pesocheque_pref";
    public static final String BASE_BLUEMIX_URL = "http://pesocheque.au-syd.mybluemix.net";
    public static final String BASE_URL = "http://pharking.com/plivo/plivo-php/";
    public static final String EMAIL = "e-mail";
    public static final String FULL_NAME = "full_name";
    public static final String BIRTH_DATE = "birth_date";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String PASSWORD = "password";
    public static final String BANK = "bank";
    public static final String ACCOUNT_NAME = "account_name";
    public static final String ACCOUNT_NUMBER = "account_number";

    public static final String QUOTIENT = "quotient";
    public static final String AMOUNT = "amount";
    public static final String MONTHS = "months";
    public static final String RECIPIENT = "recipient";
    public static final String REASONS = "reasons";

}