package com.pharking.pesocheque.utils;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by say13 on 27/11/2016.
 */

public interface API {


//    http://pharking.com/plivo/plivo-php/sendsms.php

    @GET("/index")
    Call<User> getECode();

    @GET("sendsms.php")
    Call<User> getCode();

}
