package com.pharking.pesocheque.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by say13 on 13/11/2016.
 */

public class User {


    @SerializedName("otp_code")
    public String otpCode;

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }
}