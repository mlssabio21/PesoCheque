package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pharking.pesocheque.utils.Constants;

public class SignUp2Activity extends AppCompatActivity {

    private EditText etAccountName, etAccountNumber;
    private Spinner spSelectBank;
    private Button btNext;
    private String[] banks = { "Select Bank", "UnionBank" };
    private ArrayAdapter<String> banksAdapter;

    SharedPreferences shared;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up2);
        initControls();
    }

    private void initControls() {
        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        edit = shared.edit();

        btNext = (Button) findViewById(R.id.btNext);
        etAccountName = (EditText) findViewById(R.id.etAccountName);
        etAccountNumber = (EditText) findViewById(R.id.etAccountNumber);
        spSelectBank = (Spinner) findViewById(R.id.spSelectBank);
        banksAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, banks);
        spSelectBank.setAdapter(banksAdapter);

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spSelectBank.getSelectedItemId() == 0) {
                    Toast.makeText(SignUp2Activity.this, "Select a bank first", Toast.LENGTH_SHORT).show();
                } else if(etAccountName.getText().toString().length() < 1) {
                    etAccountName.requestFocus();
                    etAccountName.setError("Please enter your account name");
                } else if (etAccountNumber.getText().toString().length() < 1) {
                    etAccountNumber.requestFocus();
                    etAccountNumber.setError("Please enter your account number");
                } else {
                    // Save details
                    edit.putString(Constants.ACCOUNT_NAME, etAccountName.getText().toString());
                    edit.putString(Constants.ACCOUNT_NUMBER, etAccountNumber.getText().toString());
                    edit.putString(Constants.BANK, spSelectBank.getSelectedItem().toString());
                    edit.commit();
                    // go to newt
                    Intent i = new Intent(SignUp2Activity.this, SignUp3Activity.class);
                    startActivity(i);
                }
            }
        });
    }

}
