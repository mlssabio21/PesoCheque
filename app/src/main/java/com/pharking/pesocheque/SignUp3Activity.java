package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pharking.pesocheque.utils.Constants;

import org.w3c.dom.Text;

public class SignUp3Activity extends AppCompatActivity {

    private TextView tvFullName, tvBirthDate, tvEmail, tvMobileNumber, tvBankName, tvAccountName, tvAccountNumber;
    private Button btRegister;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up3);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        tvFullName = (TextView) findViewById(R.id.tvFullName);
        tvBirthDate = (TextView) findViewById(R.id.tvBirthDate);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvMobileNumber = (TextView) findViewById(R.id.tvMobileNumber);
        tvBankName = (TextView) findViewById(R.id.tvBankName);
        tvAccountName = (TextView) findViewById(R.id.tvAccountName);
        tvAccountNumber = (TextView) findViewById(R.id.tvAccountNumber);
        btRegister = (Button) findViewById(R.id.btRegister);

        tvFullName.setText(shared.getString(Constants.FULL_NAME, ""));
        tvBirthDate.setText(shared.getString(Constants.BIRTH_DATE, ""));
        tvEmail.setText(shared.getString(Constants.EMAIL, ""));
        tvMobileNumber.setText(shared.getString(Constants.MOBILE_NUMBER, ""));
        tvBankName.setText(shared.getString(Constants.BANK, ""));
        tvAccountName.setText(shared.getString(Constants.ACCOUNT_NAME, ""));
        tvAccountNumber.setText(shared.getString(Constants.ACCOUNT_NUMBER, ""));

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignUp3Activity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }

}