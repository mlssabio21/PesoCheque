package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pharking.pesocheque.utils.Constants;

public class SignUp1Activity extends AppCompatActivity {

    private EditText etEmail, etFullName, etBirthDate, etMobileNumber, etPassword;
    private Button btNext;

    SharedPreferences shared;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up1);
        initControls();
    }

    private void initControls() {
        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        edit = shared.edit();

        btNext = (Button) findViewById(R.id.btNext);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etFullName = (EditText) findViewById(R.id.etFullName);
        etBirthDate = (EditText) findViewById(R.id.etBirthDate);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etFullName.getText().toString().length() < 1) {
                    etFullName.requestFocus();
                    etFullName.setError("Please enter your full name");
                } else if (etBirthDate.getText().toString().length() < 1) {
                    etBirthDate.requestFocus();
                    etBirthDate.setError("Please enter your birthdate");
                } else if(etEmail.getText().toString().length() < 1) {
                    etEmail.requestFocus();
                    etEmail.setError("Please enter your e-mail address");
                } else if (etMobileNumber.getText().toString().length() < 1) {
                    etMobileNumber.requestFocus();
                    etMobileNumber.setError("Please enter your mobile number");
                } else if (etPassword.getText().toString().length() < 1) {
                    etPassword.requestFocus();
                    etPassword.setError("Please enter your password");
                } else {
                    // Save details
                    edit.putString(Constants.EMAIL, etEmail.getText().toString());
                    edit.putString(Constants.FULL_NAME, etFullName.getText().toString());
                    edit.putString(Constants.BIRTH_DATE, etBirthDate.getText().toString());
                    edit.putString(Constants.MOBILE_NUMBER, etMobileNumber.getText().toString());
                    edit.putString(Constants.PASSWORD, etPassword.getText().toString());
                    edit.commit();
                    // Go to Main
                    Intent i = new Intent(SignUp1Activity.this, SignUp2Activity.class);
                    startActivity(i);
                }
            }
        });
    }

}