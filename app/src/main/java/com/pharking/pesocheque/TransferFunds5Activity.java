package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pharking.pesocheque.utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TransferFunds5Activity extends AppCompatActivity {

    private TextView tvContent, tvCheckHistory;
    private Button btDone;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds5);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Date date = new Date();

        tvContent = (TextView) findViewById(R.id.tvContent);
        tvCheckHistory = (TextView) findViewById(R.id.tvCheckHistory);
        btDone = (Button) findViewById(R.id.btDone);

        tvContent.setText("You have transacted this " + shared.getString(Constants.REASONS, "") + " amounting to " +
                shared.getString(Constants.AMOUNT, "") + " for " + shared.getString(Constants.MONTHS, "") + " to " +
                shared.getString(Constants.RECIPIENT, "") + " as of today " + dateFormat.format(date) + " at "
                + "Meralco Ave., Pasig City, Philippines"
        );

        tvCheckHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransferFunds5Activity.this, TransactionHistoryActivity.class);
                startActivity(i);
            }
        });

        btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransferFunds5Activity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }
}
