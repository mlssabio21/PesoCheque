package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pharking.pesocheque.utils.Constants;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btLogin;
    private EditText etEmail, etPassword;
    private TextView tvResetPassword, tvRegister;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        initControls();

    }

    private void initControls() {
        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        btLogin = (Button) findViewById(R.id.btLogin);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvResetPassword = (TextView) findViewById(R.id.tvResetPassword);
        tvRegister = (TextView) findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(this);
        tvResetPassword.setOnClickListener(this);
        btLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.tvRegister:
                Intent signUp = new Intent(this, SignUp1Activity.class);
                startActivity(signUp);
                break;
            case R.id.tvResetPassword:

                break;
            case R.id.btLogin:
                if(etEmail.getText().length() < 1) {
                    etEmail.setError(getResources().getString(R.string.error_field_required));
                    etEmail.requestFocus();
                } else if (etPassword.getText().length() < 1) {
                    etPassword.setError(getResources().getString(R.string.error_field_required));
                    etPassword.requestFocus();
                } else {
                    if(etEmail.getText().toString().equals(shared.getString(Constants.EMAIL, "")) &&
                            etPassword.getText().toString().equals(shared.getString(Constants.PASSWORD, ""))) {
                        etEmail.getText().clear();
                        etPassword.getText().clear();
                        Intent i = new Intent(this, MainActivity.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}