package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pharking.pesocheque.utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TransactionHistoryActivity extends AppCompatActivity {

    private TextView tvDate, tvTerms, tvLocation, tvAmt, tvStatus;
    private TextView tvSender, tvRCVDate, tvRCVTerms, tvRCVAmt, tvRCVStatus;
    private Button btHome;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvTerms = (TextView) findViewById(R.id.tvTerms);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvAmt = (TextView) findViewById(R.id.tvAmt);
        tvStatus = (TextView) findViewById(R.id.tvStatus);

        tvRCVDate = (TextView) findViewById(R.id.tvRCVDate);
        tvRCVTerms = (TextView) findViewById(R.id.tvRCVTerms);
        tvSender = (TextView) findViewById(R.id.tvSender);
        tvRCVAmt = (TextView) findViewById(R.id.tvRCVAmt);
        tvRCVStatus = (TextView) findViewById(R.id.tvRCVStatus);

        btHome = (Button) findViewById(R.id.btHome);

        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yy", Locale.US);
        Date date = new Date();

        tvDate.setText(dateFormat.format(date));
        tvTerms.setText(shared.getString(Constants.MONTHS, "").split("\\s")[0]);
        tvLocation.setText("Meralco Ave., Pasig City, Philippines");
        tvAmt.setText(shared.getString(Constants.AMOUNT, ""));
        tvStatus.setText("APPROVED");

        tvRCVDate.setText("10-15-16\n11-15-16\n11-27-16");
        tvRCVTerms.setText("1\n1\n1");
        tvSender.setText("Mary Sabio\nJosh Garcia\nMike Fong");
        tvRCVAmt.setText("10,000.00\n24,000.00\n13,000.00");
        tvRCVStatus.setText("CLEARED\nBOUNCED\nPENDING");

        btHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransactionHistoryActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

    }
}
