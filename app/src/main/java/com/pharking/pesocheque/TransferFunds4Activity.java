package com.pharking.pesocheque;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pharking.pesocheque.utils.API;
import com.pharking.pesocheque.utils.Constants;
import com.pharking.pesocheque.utils.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TransferFunds4Activity extends AppCompatActivity {

    private TextView tvVerified;
    private EditText etOTP;
    private Button btVerify,btNext;

    private final String TAG = "TransferFunds4Activity";

    private Gson gson;
    private API apiService;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds4);
        initControls();
    }

    private void initControls() {
        etOTP = (EditText) findViewById(R.id.etOTP);
        tvVerified = (TextView) findViewById(R.id.tvVerified);
        btVerify = (Button) findViewById(R.id.btVerify);
        btNext = (Button) findViewById(R.id.btNext);


        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiService = retrofit.create(API.class);

        sendOTP();


        btVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etOTP.getText().toString().length() < 1) {
                    etOTP.requestFocus();
                    etOTP.setError("Please enter OTP code sent to your mobile number");
                } else {
                    tvVerified.setVisibility(View.VISIBLE);
                    btVerify.setVisibility(View.GONE);
                    btNext.setVisibility(View.VISIBLE);
                }
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransferFunds4Activity.this, TransferFunds5Activity.class);
                startActivity(i);
            }
        });


    }

    private void sendOTP() {
        displayProgress();
        Call<User> call = apiService.getCode();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                int statusCode = response.code();
                hideProgress();
                Log.e(TAG, "STATUS CODE: " + statusCode);
                try {
                    if(statusCode == 200) {
                        Log.e(TAG, "RESPONSE: " + response.body());

                    } else {
                        Log.e(TAG, "MSG: " + response.errorBody().string());
//                        try {
//                            String errorBody = response.errorBody().string();
//                            JSONObject json = new JSONObject(errorBody);
//                            String message = json.getString(Constants.MESSAGE);
//                        } catch (JSONException e) {
//                            Log.e(TAG, "" + e.toString());
//                        }
                    }

                } catch (Exception e) {
                    hideProgress();
                    Log.e(TAG, "" + e.toString());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void displayProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending OTP code");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hideProgress() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
