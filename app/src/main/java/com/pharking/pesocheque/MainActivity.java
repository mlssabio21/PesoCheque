package com.pharking.pesocheque;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.pharking.pesocheque.utils.Constants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvFullName, tvTransferFunds, tvTransactionHistory, tvManageBankAccounts, tvEditProfile, tvLogOut;
    SharedPreferences shared;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControls();
    }

    private void initControls() {
        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        tvFullName = (TextView) findViewById(R.id.tvFullName);
        tvTransferFunds = (TextView) findViewById(R.id.tvTransferFunds);
        tvTransactionHistory = (TextView) findViewById(R.id.tvTransactionHistory);
        tvManageBankAccounts = (TextView) findViewById(R.id.tvManageBankAccounts);
        tvEditProfile = (TextView) findViewById(R.id.tvEditProfile);
        tvLogOut = (TextView) findViewById(R.id.tvLogOut);

        tvFullName.setText(shared.getString(Constants.FULL_NAME, "") + "!");
        tvTransferFunds.setOnClickListener(this);
        tvTransactionHistory.setOnClickListener(this);
        tvManageBankAccounts.setOnClickListener(this);
        tvEditProfile.setOnClickListener(this);
        tvLogOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent i;

        switch(view.getId()) {
            case R.id.tvTransferFunds:
                i = new Intent(this, TransferFunds1Activity.class);
                startActivity(i);
                break;
            case R.id.tvTransactionHistory:
                i = new Intent(this, TransactionHistoryActivity.class);
                startActivity(i);
                break;
            case R.id.tvManageBankAccounts:
                break;
            case R.id.tvEditProfile:
                i = new Intent(this, EditProfile1Activity.class);
                startActivity(i);
                break;
            case R.id.tvLogOut:
                // TODO clear pref
                break;
        }
    }
}