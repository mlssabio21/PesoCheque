package com.pharking.pesocheque;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pharking.pesocheque.utils.Constants;

public class TransferFunds2Activity extends AppCompatActivity implements View.OnTouchListener {

    private Spinner spSelectBank, spReasons;
    private String[] banks = { "Select Bank", "UnionBank", "BPI", "BDO", "EastWest" };
    private String[] reasons = { "Select reason for transfer", "Fund Transfer", "Payment to client", "Bills Payment",
            "Purchase of Item", "Others" };
    private ArrayAdapter<String> banksAdapter, reasonsAdapter;
    private EditText etEnterAccountName, etEnterAccountNumber;
    private Button btNext;

    SharedPreferences shared;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds2);
        initControls();
    }

    private void initControls() {

        shared = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        edit = shared.edit();

        spSelectBank = (Spinner) findViewById(R.id.spSelectBank);
        spReasons = (Spinner) findViewById(R.id.spReasons);
        banksAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, banks);
        reasonsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, reasons);
        spSelectBank.setAdapter(banksAdapter);
        spReasons.setAdapter(reasonsAdapter);

        etEnterAccountName = (EditText) findViewById(R.id.etEnterAccountName);
        etEnterAccountNumber = (EditText) findViewById(R.id.etEnterAccountNumber);
        btNext = (Button) findViewById(R.id.btNext);

        etEnterAccountName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideSoftKeyboard(v);
                }
            }
        });

        etEnterAccountNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideSoftKeyboard(v);
                }
            }
        });

        etEnterAccountName.setOnTouchListener(this);
        etEnterAccountNumber.setOnTouchListener(this);

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spSelectBank.getSelectedItemId() == 0) {
                    Toast.makeText(TransferFunds2Activity.this, "Select a bank first", Toast.LENGTH_SHORT).show();
                } else {
                    edit.putString(Constants.RECIPIENT, etEnterAccountName.getText().toString());
                    edit.putString(Constants.REASONS, spReasons.getSelectedItem().toString());
                    edit.commit();
                    Intent i = new Intent(TransferFunds2Activity.this, TransferFunds3Activity.class);
                    startActivity(i);
                }
            }
        });

    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return false;
    }
}
